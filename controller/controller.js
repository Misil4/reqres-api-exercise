import { getAllRows } from "../services/services.js";

const fetchAllRows = async () => {
    try {
        const data = await getAllRows();
        return data
    } catch (error) {
        console.log(error);
    }
}
export {fetchAllRows};