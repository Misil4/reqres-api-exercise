import axios from "axios";

const getAllRows = async () => {
    return axios.get('https://reqres.in/api/unknown')
        .then((response) => response.data)
        .catch((error) => console.log(error));
}
export { getAllRows };